﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Memory
{
    public class Board
    {
        public Card[] Cards { get; }

        public Player[] Players { get; }

        private int _activePlayerIndex;

        private List<Card> _visibleCards;

        public int VisibleCardsCount { get => _visibleCards.Count; }

        public Board(int playerCount, int imageCount)
        {
            Players = new Player[playerCount];
            Cards = new Card[imageCount * 2];
            _visibleCards = new List<Card>();
        }

        public void MixCards()
        {
            Random randomNumberGenerator = new Random();
            // For every image...
            for (int imageNumber = 1; imageNumber <= Cards.Length / 2; imageNumber++)
            {
                Card matchingCard = null;
                // ... do twice
                for (int i = 0; i < 1; i++)
                {
                    do
                    {
                        int position = randomNumberGenerator.Next(0, Cards.Length);
                        if (Cards[position] == null)
                        {
                            Card card = new Card(position, imageNumber);

                            if (matchingCard == null)
                            {
                                matchingCard = card;
                            }
                            else
                            {
                                matchingCard.MatchingCard = card;
                                card.MatchingCard = matchingCard;
                            }
                            Cards[position] = card;
                        }
                    } while (matchingCard == null || matchingCard.MatchingCard == null);
                }
            }
        }

        public void StartGame()
        {
            Random randomNumberGenerator = new Random();
            _activePlayerIndex = randomNumberGenerator.Next(0, 2);
        }

        private void SwitchPlayer()
        {
            _activePlayerIndex = ++_activePlayerIndex % 2;
        }

        public void FlipCard(int position)
        {
            Card card = Cards[position];
            if (card.Flip())
            {
                _visibleCards.Add(card);
                if (VisibleCardsCount == 2)
                {
                    CompareCards();
                }
            }
        }

        private void CompareCards()
        {
            if (_visibleCards[0].ImageNumber == _visibleCards[1].ImageNumber)
            {
                // It's a match
                Players[_activePlayerIndex].Points++;
                HideVisibleCards();

                if (Players[0].Points + Players[1].Points == Cards.Length / 2)
                {
                    MessageBox.Show("Spiel ist fertig");
                }
            }
            else
            {
                SwitchPlayer();
            }
        }

        public void CoverVisibleCards()
        {
            foreach (Card card in _visibleCards)
            {
                card.Flip();
            }
            _visibleCards.Clear();
        }

        private void HideVisibleCards()
        {
            foreach (Card card in _visibleCards)
            {
                card.Button.Visibility = Visibility.Hidden;
            }
            _visibleCards.Clear();
        }
    }
}
