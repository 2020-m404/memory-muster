﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Memory
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int ImageCount = 18;
        private const int PlayerCount = 2;
        private const int ButtonDimension = 100;
        private const int ColCount = 6;
        private const int RowCount = 6;

        private Board _board;

        public MainWindow()
        {
            InitializeComponent();
            _board = new Board(PlayerCount, ImageCount);
        }

        public void StartGame(object sender, RoutedEventArgs e)
        {
            if (!InitializePlayers())
            {
                MessageBox.Show("Geben Sie für beide Spieler einen Namen mit maximal 20 Zeichen ein!", "Spielernamen definieren");
                return;
            }

            _board.MixCards();
            DrawCards();
            _board.StartGame();
        }

        private bool InitializePlayers()
        {
            string player1Name = Player1Name.Text;
            string player2Name = Player2Name.Text;

            if (string.IsNullOrEmpty(player1Name) || player1Name.Length > 20)
            {
                return false;
            }

            if (string.IsNullOrEmpty(player2Name) || player2Name.Length > 20)
            {
                return false;
            }

            _board.Players[0] = new Player(player1Name);
            _board.Players[1] = new Player(player2Name);

            return true;
        }

        private void DrawCards()
        {
            foreach (Card card in _board.Cards)
            {
                Button button = new Button
                {
                    Width = ButtonDimension,
                    Height = ButtonDimension,
                    Tag = card.Position,
                    Content = new Image
                    {
                        Source = new BitmapImage(new Uri(@"MemoryImages\BackSide.png", UriKind.Relative))
                    }
                };

                button.Click += OnButtonClick;

                Grid.SetRow(button, card.Position / ColCount);
                Grid.SetColumn(button, card.Position % ColCount);

                card.Button = button;

                PlayingField.Children.Add(button);
            }
        }

        private void OnButtonClick(object sender, RoutedEventArgs e)
        {
            Button button = (Button)sender;
            if (_board.VisibleCardsCount < 2)
            {
                _board.FlipCard(int.Parse(button.Tag.ToString()));
            }
            else
            {
                _board.CoverVisibleCards();
            }
        }
    }
}
