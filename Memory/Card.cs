﻿using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace Memory
{
    public class Card
    {
        public int Position { get; }
        public int ImageNumber { get; }
        private bool _isVisible;
        public Button Button { get; set; }
        public Card MatchingCard { get; set; }

        public Card(int position, int imageNumber)
        {
            Position = position;
            ImageNumber = imageNumber;
        }

        public bool Flip()
        {
            string imageName;
            if (!_isVisible)
            {
                imageName = $"MemoryImage_{ImageNumber}.png";
            }
            else
            {
                imageName = "BackSide.png";
            }

            Button.Content = new Image
            {
                Source = new BitmapImage(new Uri($@"MemoryImages/{imageName}", UriKind.Relative))
            };

            _isVisible = !_isVisible;
            return _isVisible;
        }
    }
}
