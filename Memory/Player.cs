﻿namespace Memory
{
    public class Player
    {
        public string Name { get; }
        public int Points { get; set; } = 0;

        public Player(string name)
        {
            Name = name;
        }
    }
}
